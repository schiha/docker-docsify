<!-- _coverpage.md -->

![logo](_media/logo.svg)

# digbooks <small>1.0</small>

> A magical documentation site generator.

- Simple and lightweight (~21kB gzipped)
- No statically built html files
- Multiple themes

[GitHub](https://github.com/docsifyjs/docsify/)
[Get Started](#docsify)

<!-- background color -->

![color](#f0f0f0)